
# Ejercicio 14.6: calculadora.py

def sumar(x: int, y: int) -> int:
    """Sumar dos numeros enteros"""
    return x + y


def restar(x: int, y: int) -> int:
    """ restar dos numeros enteros"""
    return x - y


# Creamos un banco de pruebas

test_numbers: list = [1, 3, 5, 7]

for number in test_numbers:
    if number <= 3:
        print(number, "+", number + 1, "=", sumar(number, number + 1))
    else:
        print(number + 1, "-", number, "=", restar(number + 1, number))
